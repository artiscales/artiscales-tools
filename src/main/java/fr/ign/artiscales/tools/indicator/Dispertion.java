package fr.ign.artiscales.tools.indicator;

import java.util.List;

public class Dispertion {
    public static double gini(List<Double> values) {
        return values.stream().flatMapToDouble(v1 -> values.stream().mapToDouble(v2 -> Math.abs(v1 - v2))).sum() / (2 * values.size() * values.size() * values.stream().mapToDouble(v -> v).average().getAsDouble());
    }
}
