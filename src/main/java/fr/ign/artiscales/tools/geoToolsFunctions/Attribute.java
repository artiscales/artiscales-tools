package fr.ign.artiscales.tools.geoToolsFunctions;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Classes to deal with collection's attributes
 */
public class Attribute {
//    public static void main(String[] args) {
//        long start = System.currentTimeMillis();
//        for (int i = 0 ; i <= 1500000 ; i ++)
//          makeUniqueId();
//
//        ASTools.print((System.currentTimeMillis() - start));
//    }

    /**
     * Generate a very unique ID number
     *
     * @return a very unique ID number
     */
    public static String makeUniqueId() {
//     return  RandomStringUtils.random(14, true, true);
        return String.valueOf((int) (Math.random() * 100)) + System.nanoTime();
    }

    /**
     * Get the indice number on the position of the Latitude field from a .csv
     *
     * @param head the header of the .csv file
     * @return the indice on which number
     */
    public static int getLatIndice(String[] head) throws FileNotFoundException {
        for (int i = 0; i < head.length; i = i + 1) {
            if (head[i].toLowerCase().contains("latitude") || head[i].equalsIgnoreCase("lat") || head[i].equalsIgnoreCase("latitude")
                    || head[i].equalsIgnoreCase("x") || head[i].equalsIgnoreCase("lambert_x")
                    || (head[i].toLowerCase().contains("x") && head[i].toLowerCase().contains("coord")))
                return i;
        }
        throw new FileNotFoundException("Attribute.getLatIndice : no latitude indice found");
    }

    /**
     * Get the indice number on the position of the Latitude field from a .csv
     *
     * @param head the header of the .csv file
     * @return the indice on which number
     */
    public static int getLongIndice(String[] head) throws FileNotFoundException {
        for (int i = 0; i < head.length; i = i + 1) {
            if (head[i].toLowerCase().contains("longitude") || head[i].equalsIgnoreCase("lon") || head[i].equalsIgnoreCase("long")
                    || head[i].equalsIgnoreCase("y") || head[i].equalsIgnoreCase("lambert_y")
                    || (head[i].toLowerCase().contains("y") && head[i].toLowerCase().contains("coord")))
                return i;
        }
        throw new FileNotFoundException("Attribute.getLongIndice : no longitude indice found");
    }

    /**
     * Get the indice number representing the position of a field representing city or zip code from a .csv
     *
     * @param head the header of the .csv file
     * @return the indice on which number
     */
    public static int getCityCodeIndice(String[] head) throws FileNotFoundException {
        for (int i = 0; i < head.length; i = i + 1) {
            String word = head[i].toLowerCase();
            if (word.contains("codepos") || word.contains("code_post") || word.contains("zipcode") || head[i].toLowerCase().contains("insee")
                    || head[i].toLowerCase().contains("code_insee") || head[i].toLowerCase().contains("depcom")
                    || head[i].toLowerCase().contains("codecommune"))
                return i;
        }
        throw new FileNotFoundException("Attribute.getZipCodeIndice : no zipcode indice found");
    }

    /**
     * Get the indice number representing the position of the given field.
     * Read the next line of the given reader
     *
     * @param r          a .csv reader at its initial position. Will not rewind the reader
     * @param indiceName Full and complete name of the header
     * @return the indice on which number
     */
    public static int getIndice(CSVReader r, String indiceName) {
        try {
            return getIndice(r.readNext(), indiceName);
        } catch (IOException | CsvValidationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the indice number representing the position of the given field.
     *
     * @param head       the header of the .csv file
     * @param indiceName Full and complete name of the header
     * @return the indice on which number
     */
    public static int getIndice(String[] head, String indiceName) throws FileNotFoundException {
        for (int i = 0; i < head.length; i = i + 1)
            if (head[i].equals(indiceName))
                return i;
        throw new FileNotFoundException("Attribute.getIndice : no " + indiceName + " indice found");
    }
}
