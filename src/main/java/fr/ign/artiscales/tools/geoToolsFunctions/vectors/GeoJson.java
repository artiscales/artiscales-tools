package fr.ign.artiscales.tools.geoToolsFunctions.vectors;

import fr.ign.artiscales.tools.io.Json;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.geojson.GeoJSON;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.io.IOException;

public class GeoJson extends Json {

    public static SimpleFeatureCollection getGeoJSONReader(File geojsonFile) throws IOException {
        return (SimpleFeatureCollection) org.geotools.geojson.GeoJSON.read(geojsonFile);
    }

    public static File exportSFCtoGJSON(SimpleFeatureCollection toExport, File fileOut, SimpleFeatureType ft, boolean overwrite) throws IOException {
        GeoJSON.write(toExport, fileOut);
        return fileOut;
    }
}
