# Version 1.3

## add 
* Age pyramid for each french region (data and associated methods)
* Various basic function ( mergeGpkgOnDifferentLayers, voronoï polygons, csv functions, gini indicator)

## Change
* Downgrade to Java11 for OpenMole compatibility
* Every print goes through a function that can be silented
* Automatic ID are a suit of numbers (which is way faster)
* Various function (JoinCSVToGeoFile, mergeGpkgFiles,getClostestFeat,selectWhichIntersectMost)
* Refactor CSV functions - some functions does not put it into memory
* SRC put in parent Class


## Fix 

* Various function (safe geometric operation in geoToolsFunctions.vectors.Geom, refactor Geopackages.getDataStore)
